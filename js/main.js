//Теория
// Потому что в input существуют другие способы ввода, кроме как с помощью клавиатуры, и поэтому, если мы хотим, чтобы ввод был корректным, то лучше использовать специальные, для этого действия, события, а не события клавиатуры.

//Задание
const keys = document.querySelectorAll('.btn');

document.addEventListener('keypress', (evt) => {
    keys.forEach(i => {
        i.style.backgroundColor = '';
        if(evt.key === i.textContent || evt.key.toUpperCase() === i.textContent) {
            i.style.backgroundColor = 'blue';
        }
    });
});
